# Curso Analisis de Datos Basico con Python

![](Banner_Curso.png)

El análisis de datos y Big Data es un campo que está siendo requerido en la actualidad cada vez mayor, la razón de ser para este campo consiste en ordenar, transformar, estructurar y mejorar el manejo de los datos en un ambiente laboral o investigador. Este curso es una introducción a los métodos más famosos para el análisis de los datos.

Los temas vistos en el curso son:
1. Introduccion a Colaboratory, Numpy y Pandas.
2. Conceptos teoricos para el analisis de datos.
3. Conceptos fundamentales estadisticos teoricos como practicos.
4. Transformaciones basicas sobre los datos.
5. Inferencia realizada sobre los datos.